<?php

class Solution
{

    /**
     * To get the full JSON of time chunks of the provided initial period
     * @param $initialPeriod
     * @return string
     */
    public function getFullJSON($initialPeriod)
    {
        $timeChunks = $this->getTimeChunks([strtotime($initialPeriod[0]), strtotime($initialPeriod[1])]);
        
        return json_encode(['initialPeriod' => $initialPeriod, 'timeChunks' => $timeChunks]);
    }

    /**
     * To get the time chunks the provided initial period
     * @param $timePeriod
     * @param $interval | Hours | 30 Days by default
     * @return array
     */
    private function getTimeChunks($timePeriod, $interval = 720)
    {
        $startTime = $timePeriod[0];
        $endTime = $timePeriod[1];
        $timeChunks = [];
        while($startTime <= $endTime){
            $chunkEndTime = strtotime("+{$interval} hour", $startTime);
            $chunkEndTime = ($chunkEndTime > $endTime) ? $endTime : $chunkEndTime;
            $timeChunk = [$startTime, $chunkEndTime];
            if(!$this->getUserDataViaAPI($timeChunk) && $interval >= 2){
                // Recursive function call if DLPU reached and interval split is not less than minimum
                $subTimeChunks = $this->getTimeChunks($timeChunk, round($interval/2));
                $timeChunks = array_merge($timeChunks, $subTimeChunks);
            }
            else{
                $timeChunk[0] = date('Y-m-d H:i:s', $timeChunk[0]);
                $timeChunk[1] = date('Y-m-d H:i:s', $timeChunk[1]);
                $timeChunks[] = $timeChunk;
            }
            $offset = $interval * 60 * 60 + 1;
            $startTime = strtotime("+{$offset} second", $startTime);
        }

        return $timeChunks;
    }

    /**
     * API Call to get user data for a particular period
     * @param $timeChunk
     * @return boolean | false is returned if the DLPU is reached | rand() method used for testing
     */
    private function getUserDataViaAPI($timeChunk)
    {
        return (bool)rand(0,5);
    }

}